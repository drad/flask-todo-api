import Vue from 'vue'
import axios from 'axios'
import { LocalStorage } from 'quasar'

Vue.prototype.$axios = axios

axios.defaults.baseURL = process.env.API_SERVER + ':' + process.env.API_PORT + '/' + process.env.API_BASE_PATH

// Add a request interceptor
axios.interceptors.request.use(function (config) {
  // if route is 'protected', make sure we have a bearer token
  // console.log('  - route: ' + config.url)
  if (!config.url.includes('login')) {
    // console.log('- token: ', LocalStorage.getItem('access_token'))
    // check for bearer token.
    if (LocalStorage.getItem('access_token')) {
      //console.log('- have token stored, add it to axios headers...')
      // add auth_token to header.
      axios.defaults.headers.common['Authorization'] = `Bearer ${LocalStorage.getItem('access_token')}`
      config.headers['Authorization'] = `Bearer ${LocalStorage.getItem('access_token')}`
    } else {
      // let this go through and the response interceptor will handle it.
      // console.log('- NO TOKEN')
    }
  } else {
    // console.log('- something else happened...')
  }
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

// Add a response interceptor
axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  // Prevent endless redirects (login is where you should end up)
  if (error.request !== undefined) {
    if (error.request.responseURL.includes('login')) {
      return Promise.reject(error)
    }
  }
  // console.log('- Response Interceptor check: ', error.response.config.url)
  if (error.response.status === 401 && !error.response.config.url.includes('login')) {
    // console.log('- interceptor found a 401...')
    // router().push('/login')
    window.location.href = '#/login'
    // console.log('- after redirect...')
  } else {
    // console.log('- skipped')
  }
})
