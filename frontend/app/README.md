# todo (todo)

a todo app

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).



## Development ##

If you need/want to change the app icon(s) you can use icon-genie (a quasar tool for doing this).

As we dont change the app icon often, we keep this a manual task:

- connect to frontend container: `docker-compose exec frontend ash`
- install needed packages: `apk add --no-cache gcc g++ make libpng-dev`
- install icon-genie: `quasar ext add @quasar/icon-genie`
- you can remove as follows: `quasar ext remove @quasar/icon-genie`
  - keep in mind you are in a container so its often easier/cleaner to simply delete the container and recreate it and all of the icon-genie components will be gone
