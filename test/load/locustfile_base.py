import datetime
from locust import HttpLocust, TaskSet, task, between

# install: `pip install locustio`
# start server: `locust -f locustfile_base.py`
# run/monitor test: http://localhost:8089/
#

import csv
import json
import logging
import random

USERS = None
TODOS = None
about = {
    "name": "flask-todo-api-load-base",
    "version": "1.1.1",
    "modified": "2020-01-01",
    "created": "2020-01-01",
}


def getTodo():
    todo = None
    priority = None
    note = None
    if len(TODOS) > 0:
        (todo, priority, note) = TODOS.pop(
            random.randrange(len(TODOS))  # nosec
        )  # pop random item from list
    else:
        # if we have no more TODOS use a dummy.
        logging.warn("NOTICE: test ran out of TODOs, using dummy todo...")
        todo = "Dummy TODO Record"
        priority = 7
        note = "This is a dummy todo record which is used when the test script runs out of TODOs to use. Dummy records can be avoided by increasing the number of TODOs in the todos.csv file."
    return todo, priority, note


class WebsiteTasksBasic(TaskSet):
    user = "Undefined"
    headers = {"Content-Type": "application/json", "Cache-Control": "no-cache"}
    payload = {}

    def on_start(self):
        if len(USERS) > 0:
            self.user = USERS.pop()[0]
        logging.debug(f"- user set to: {self.user}")

    # @seq_task(1)
    @task(2)
    def add_edit_delete(self):
        (
            self.payload["todo"],
            self.payload["priority"],
            self.payload["note"],
        ) = getTodo()
        self.payload["owner"] = self.user
        logging.debug(f"- running as user: {self.user}")
        # logging.info(f"- initial payload: {payload_add}")
        with self.client.post(
            "/add", data=json.dumps(self.payload), headers=self.headers
        ) as add_response:
            if add_response.content:
                # edit
                # logging.info(f"- add response: {add_response.content}")
                add_resp = json.loads(add_response.content)
                rid = add_resp["id"]
                # logging.info(f"- add response rid: {rid}")
                # get
                self.client.get(f"/{rid}")
                # http -j PATCH :7999/edit/1 id=1 todo='Build an App' owner='drad' priority=4 note='Please make it a /cool/ app!' completed='2019-12-31 12:58:55'
                self.payload["completed"] = datetime.datetime.now().strftime(
                    "%Y-%m-%d %H:%M"
                )
                # logging.info(f"- payload is now: {payload_add}")
                with self.client.patch(
                    f"/edit/{rid}", data=json.dumps(self.payload), headers=self.headers
                ) as edit_response:
                    if edit_response.content:
                        # get
                        self.client.get(f"/{rid}")
                        # delete
                        # logging.info(f"- edit response: {edit_response.content}")
                        # http -j DELETE :7999/remove/1
                        with self.client.delete(f"/remove/{rid}") as delete_response:
                            if delete_response.content:
                                # logging.info(f"- delete response: {delete_response.content}")
                                pass

    # @seq_task(2)
    @task(5)
    def add_edit(self):
        (
            self.payload["todo"],
            self.payload["priority"],
            self.payload["note"],
        ) = getTodo()
        self.payload["owner"] = self.user
        logging.debug(f"- running as user: {self.user}")
        # logging.info(f"- initial payload: {payload_add}")
        with self.client.post(
            "/add", data=json.dumps(self.payload), headers=self.headers
        ) as add_response:
            if add_response.content:
                # edit
                # logging.info(f"- add response: {add_response.content}")
                add_resp = json.loads(add_response.content)
                rid = add_resp["id"]
                # logging.info(f"- add response rid: {rid}")
                # get
                self.client.get(f"/{rid}")
                # http -j PATCH :7999/edit/1 id=1 todo='Build an App' owner='drad' priority=4 note='Please make it a /cool/ app!' completed='2019-12-31 12:58:55'
                self.payload["completed"] = datetime.datetime.now().strftime(
                    "%Y-%m-%d %H:%M"
                )
                # logging.info(f"- payload is now: {payload_add}")
                with self.client.patch(
                    f"/edit/{rid}", data=json.dumps(self.payload), headers=self.headers
                ) as edit_response:
                    if edit_response.content:
                        # get
                        self.client.get(f"/{rid}")

    # @seq_task(3)
    @task(4)
    def query_my(self):
        # - get all: http :7999/user/drad
        self.client.get(f"/user/{self.user}")

    # @seq_task(4)
    @task(4)
    def query_my_limit(self):
        # - get with limit: http -j GET :7999/user/drad amount=10
        self.payload["amount"] = 10
        self.client.get(
            f"/user/{self.user}", data=json.dumps(self.payload), headers=self.headers
        )

    # @seq_task(5)
    @task(1)
    def query_search(self):
        # http -j GET :7999/search todo='Build%'
        self.payload["todo"] = "%multi%"
        self.client.get("/search", data=json.dumps(self.payload), headers=self.headers)

    @task(6)
    def query_search_my(self):
        # http -j GET :7999/search/drad todo='Build%'
        self.payload["todo"] = "%multi%"
        self.client.get(
            f"/search/{self.user}", data=json.dumps(self.payload), headers=self.headers
        )


class WebsiteUser(HttpLocust):
    task_set = WebsiteTasksBasic
    # ~ host = "http://localhost:7999/flask-todo-api"
    host = "https://apps.dradux.com/flask-todo-api"

    wait_time = between(5, 15)  # between 5 and 15 seconds

    def __init__(self):
        super(WebsiteUser, self).__init__()
        global USERS, TODOS
        if USERS is None:
            with open("users.csv", "r") as f:
                # has_header = csv.Sniffer().has_header(f.read(1024))
                # f.seek(0)  # Rewind.
                reader = csv.reader(f)
                # if has_header:
                next(reader)  # Skip header row.
                USERS = list(reader)
            logging.info(f"- loaded USERS ({len(USERS)})")
        if TODOS is None:
            with open("todos.csv", "r") as f:
                has_header = csv.Sniffer().has_header(f.read(1024))
                f.seek(0)  # Rewind.
                reader = csv.reader(f)
                if has_header:
                    next(reader)  # Skip header row.
                TODOS = list(reader)
            logging.info(f"- loaded TODOS ({len(TODOS)})")
