# README

Load testing info.


## Notices ##

- requires pythin => 3.5 (fstrings)
- requires locust => 0.13.5
- please ensure the `users.csv` file has (at least) the number of users you will send in from locust
  - the default `users.csv` ships with 1000 users (see Tips section for generating more if you need)
- the locust script __creates and leaves__ data in your database, no existing data will be modified or deleted


## Setup ##

- create virtualenv: `python3 -m virtualenv .venv`
- activate env: `source .venv/bin/activate`
- install locust: `pip install locustio`


## Run ##

- start locust: `locust -f locustfile_base.py`
- go to [web ui](http://localhost:8089/) to start/monitor load test


## Tips ##

You can generate your own users and todos by going to [mockaroo](https://mockaroo.com/schemas/download). The project files were created as follow:

users.csv
  - # Rows: 1000
  - user
    - type: username
    - blank: 0%
todos.csv
  - # Rows: 10000
  - phrase
    - type: Catch Phrase
    - blank: 0%
  - priority
    - type: Number
    - min: 1
    - max: 25
    - decimals: 0
    - blank: 0%
  - note
    - type: Words
    - at least: 5
    - but no more than: 500
    - blank: 3%
