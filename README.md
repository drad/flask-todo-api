# README

## About ##

This is a simple API CRUD application, its purpose is to provide something to hit that generates database load which can be used to load/stress test a database (not that there is much debate for ability to handle load at the database level but the underlying infrastructure is the target - e.g. disk i/o, server specs, network latency, etc.).


## Architecture ##

Simple.

- app: flask
- database: postgresql
- locust.io (optional): load testing tool to test the app with

The app is dockerized and has a docker compose file, its also setup for k8s which is the primary usage target.


## Notices ##

- there is no web/proxy container as its really not needed for the intended use-case


## Requriements ##

- docker / docker compose


## Run ##

- create `.env` file: `cp .env-template .env`
- edit `.env` file as needed (should not need to edit it)
- start db: `docker-compose up -d db`
- start api: `docker-compose up -d api`
- start frontend: `docker-compose up -d frontend`


## Use ##

The following examples use [httpie](https://httpie.org/), curl amongst many other tools will work fine.
```
server=http://localhost:7999
base_path=flask-todo-api
```
- authenticate: `export ACCESS_TOKEN=$(http -j POST ${server}/${base_path}/login username=user1 password=abcxyz | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["access_token"]')`
- add item: `http -j POST ${server}/${base_path}/add todo='Build an App' owner='drad' priority=5 note='Please make it a /cool/ app!'`
- get all: `http -j GET ${server}/${base_path}/`
- get one: `http -j GET ${server}/${base_path}/1`
- get my: `http -j GET ${server}/${base_path}/user "Authorization:Bearer ${ACCESS_TOKEN}"`
- update: `http -j PATCH ${server}/${base_path}/edit/1 id=1 todo='Build an App' owner='drad' priority=4 note='Please make it a /cool/ app!'`
  - note: id will be ignored
- search: `http -j GET ${server}/${base_path}/search todo='Build%'`
- delete item: `http -j DELETE ${server}/${base_path}/remove/1`


## Load Testing ##

Any REST-based load testing tool would work, there is a [locust script](test/load/locustfile_base.py) available for load testing (see file or its [README](test/load/README.md) for more info).


## Links ##
* [[flask-jwt-extended|https://flask-jwt-extended.readthedocs.io/en/stable/basic_usage/]]
