import graypy
import json
import logging
import os
import os.path as op
import uuid
import werkzeug

from flask import request, jsonify
from flask_cors import CORS
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    create_access_token,
    get_jwt_identity,
)

import database as database
import config as Config

import auth.ldap as auth_ldap
import auth.local as auth_local
from utils import create_app, PrefixMiddleware
from models import Todos, LoginResponse
from werkzeug.contrib.fixers import ProxyFix

about = {
    "name": "flask-todo-api",
    "version": "1.3.0",
    "modified": "2020-01-25",
    "created": "2020-01-01",
}
service_id = str(uuid.uuid4())[:8]


app = create_app()
CORS(app)
app.config[
    "JWT_SECRET_KEY"
] = Config.DefaultConfig.JWT_SECRET_KEY  # TEMPORARY FOR JWT Work
app.config[
    "JWT_ACCESS_TOKEN_EXPIRES"
] = Config.DefaultConfig.JWT_ACCESS_TOKEN_EXPIRES  # TEMPORARY FOR JWT Work
jwt = JWTManager(app)  # TEMPORARY FOR JWT Work

logger_base = logging.getLogger("main_logger")
logger_base.setLevel(logging.getLevelName(Config.DefaultConfig.APP_LOGLEVEL))
graylog_handler = graypy.GELFUDPHandler(
    host=Config.DefaultConfig.GRAYLOG_HOST, port=Config.DefaultConfig.GRAYLOG_PORT
)
console_handler = logging.StreamHandler()
if "graylog" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(graylog_handler)
    pass
if "console" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("main_logger"),
    {
        "application_name": about["name"],
        "application_env": Config.DefaultConfig.DEPLOY_ENV,
    },
)

if app.config["DEBUG"]:
    logger.info("### App Started With Debug On ###")
logger.info(
    f"{about['name']} - v.{about['version']} ({about['modified']}) - {service_id} - {Config.DefaultConfig.APP_LOGLEVEL}"
)
logger.debug(
    f"- db name={Config.DefaultConfig.DB_NAME}, user={Config.DefaultConfig.DB_USER}, service={Config.DefaultConfig.DB_SERVICE}, port={Config.DefaultConfig.DB_PORT}"
)


# performs login and returns jwt token.
@app.route("/login", methods=["POST"])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get("username", None)
    password = request.json.get("password", None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    login_response = LoginResponse(
        f"error",
        "Unhandled Authentication Method: {Config.DefaultConfig.AUTHENTICATION_METHOD}",
        None,
    )
    if Config.DefaultConfig.AUTHENTICATION_METHOD == "ldap":
        login_response = auth_ldap.login(username, password)
    elif Config.DefaultConfig.AUTHENTICATION_METHOD == "local":
        login_response = auth_local.login(username, password)

    if login_response.status == "error":
        return jsonify(login_response.to_json()), 401

    access_token = create_access_token(identity=login_response.user.username)
    return (
        jsonify(
            access_token=access_token,
            username=login_response.user.username,
            avatar=login_response.user.avatar,
        ),
        200,
    )


@app.route("/", methods=["GET"])
@jwt_required
def fetch_all():
    """Get all or, if amount is given, limit to amount.
      - get all: http -j GET :7999/
      - get with limit: http -j GET :7999 amount=10
    """
    current_user = get_jwt_identity()
    logger.debug(f"- get all: {current_user}")
    if current_user != "admin":
        return {"status": "fail - not authorized", "username": current_user}, 401
    amount = None
    try:
        data = request.get_json()
        amount = data["amount"]
    except (
        json.decoder.JSONDecodeError,
        werkzeug.exceptions.BadRequest,
        TypeError,
    ):
        pass
    rs = database.get_all(Todos, amount=amount)
    items = []
    for item in rs:
        items.append(item.to_json())
    return json.dumps(items), 200


@app.route("/<id>", methods=["GET"])
@jwt_required
def fetch_one(id):
    """Get one (by id)
      - http -j GET :7999/1
    """
    current_user = get_jwt_identity()
    logger.debug(f"- get one: {current_user}")
    rs = database.get_one(Todos, id=id)
    if rs and rs["owner"] == current_user:
        return rs, 200
    else:
        return {"status": "fail - not found", "id": int(id)}, 404


@app.route("/user", methods=["GET"])
@jwt_required
def fetch_for_user():
    """Get all items (or if amount is given, limit to amount) for current (authenticated) user.
      - get all: http :7999/user
      - get with limit: http -j GET :7999/user amount=10
    """
    current_user = get_jwt_identity()
    logger.debug(f"- get for user: {current_user}")
    amount = None
    try:
        data = request.get_json()
        amount = data["amount"]
    except (
        json.decoder.JSONDecodeError,
        werkzeug.exceptions.BadRequest,
        TypeError,
    ):
        pass
    rs = database.get_for_user(Todos, user=current_user, amount=amount)
    items = []
    for item in rs:
        items.append(item.to_json())
    return json.dumps(items), 200


@app.route("/search", methods=["GET"])
@jwt_required
def search_for_user():
    """Search for items (search todo only, wildcard supported, case insensitive) for current user.
      - http -j GET :7999/search/drad todo='%multi%'
    """
    current_user = get_jwt_identity()
    logger.debug(f"- search for user: {current_user}")
    data = request.get_json()
    todo = data["todo"]
    rs = database.search_for_user(Todos, user=current_user, todo=todo)
    items = []
    for item in rs:
        items.append(item.to_json())
    return json.dumps(items), 200


@app.route("/add", methods=["POST"])
@jwt_required
def add():
    """Add an item
      - http -j POST :7999/add todo='Build an App' owner='drad' priority=5 note='Please make it a /cool/ app!'
    """
    current_user = get_jwt_identity()
    logger.debug(f"- add for user: {current_user}")
    data = request.get_json()
    id = database.add_instance(
        Todos,
        todo=data["todo"],
        owner=current_user,
        priority=data["priority"],
        note=data["note"],
    )
    return json.dumps({"status": "success", "id": id}), 200


@app.route("/edit/<id>", methods=["PATCH"])
@jwt_required
def edit(id):
    """Edit an item
      - http -j PATCH :7999/edit/1 id=1 todo='Build an App' owner='drad' priority=4 note='Please make it a /cool/ app!' completed='2019-12-31 12:58:55'
    """
    current_user = get_jwt_identity()
    logger.debug(f"- edit for user: {current_user}")
    data = request.get_json(force=True)
    if data:
        data["owner"] = current_user
    database.edit_instance(Todos, id=id, data=data)
    return json.dumps({"status": "success", "id": int(id)}), 200


@app.route("/remove/<id>", methods=["DELETE"])
@jwt_required
def remove(id):
    """Delete an item
      - http -j DELETE :7999/remove/1
    """
    # check to ensure the current user is the owner before deleting.
    current_user = get_jwt_identity()
    logger.debug(f"- delete for user: {current_user}")
    status = "fail"
    r = fetch_one(id)[0]  # note: get the payload rather than payload, status
    if r["owner"] == current_user:
        database.delete_instance(Todos, id=id)
        status = "success"
    else:
        logger.error("Current user is not owner, cannot delete!")
    return json.dumps({"status": status, "id": int(id)}), 200


app.wsgi_app = PrefixMiddleware(
    ProxyFix(app.wsgi_app), prefix=Config.DefaultConfig.API_BASE_URL
)

if __name__ == "__main__":
    app_dir = op.realpath(os.path.dirname(__file__))
    app.run()
