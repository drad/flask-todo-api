import os


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


class BaseConfig(object):

    API_DEBUG = env_strtobool(os.getenv("API_DEBUG", "False"))

    DEPLOY_ENV = os.getenv("DEPLOY_ENV", "prd")

    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY", "bI654OR878613b1738K77sH6cj86V6aj1")
    JWT_ACCESS_TOKEN_EXPIRES = int(
        os.getenv("JWT_ACCESS_TOKEN_EXPIRES", 900)
    )  # time in seconds (900=15m)

    # Authentication Method: local|ldap
    AUTHENTICATION_METHOD = os.getenv("AUTHENTICATION_METHOD", "ldap")
    # ldap
    LDAP_HOST = os.getenv("LDAP_HOST", None)
    LDAP_PORT = int(os.getenv("LDAP_PORT", "636"))
    LDAP_USE_SSL = env_strtobool(os.getenv("LDAP_USE_SSL", "true"))
    # The RDN attribute for your user schema on LDAP
    LDAP_USER_RDN_ATTR = "cn"
    # The Attribute you want users to authenticate to LDAP with.
    LDAP_USER_LOGIN_ATTR = "cn"
    # user attributes to get.
    # LDAP_GET_USER_ATTRIBUTES = ldap3.ALL_ATTRIBUTES
    # search with bind user.
    # ~ LDAP_ALWAYS_SEARCH_BIND = True
    LDAP_ALWAYS_SEARCH_BIND = False
    # The Username to bind to LDAP with
    LDAP_BIND_USER_DN = os.getenv("LDAP_BIND_USER_DN", None)
    # The Password to bind to LDAP with
    LDAP_BIND_USER_PASSWORD = os.getenv("LDAP_BIND_USER_PASSWORD", None)
    # Base DN of your directory
    LDAP_BASE_DN = os.getenv("LDAP_BASE_DN", None)
    # Users DN to be prepended to the Base DN (ou=users)
    # ~ LDAP_USER_DN = 'ou=users'
    LDAP_USER_DN = ""
    # Group settings
    # NOTICE: keep the following to False; we do not use Flask-Ldap3-Login's group search as something dont work with it, rather we use ldap3 directly.
    LDAP_SEARCH_FOR_GROUPS = False
    # Groups DN to be prepended to the Base DN
    # LDAP_GROUP_DN = 'ou=groups'
    # LDAP_GROUP_OBJECT_FILTER = '(objectClass=groupOfNames)'
    # LDAP_GROUP_OBJECT_FILTER = None
    # LDAP_GROUP_MEMBERS_ATTR = 'member'
    # Group scope ([LEVEL], SUBTREE) there are other levels (base/object, subordinates
    # LDAP_GROUP_SEARCH_SCOPE = 'LEVEL'
    LDAP_APP_GROUP_PREFIX = os.getenv("LDAP_APP_GROUP_PREFIX", None)

    # logging
    LOG_TO = os.getenv("LOG_TO", "console").split(",")
    APP_LOGLEVEL = os.getenv("LOG_LEVEL", "DEBUG")
    GRAYLOG_HOST = os.getenv("GRAYLOG_HOST", None)
    GRAYLOG_PORT = int(os.getenv("GRAYLOG_PORT", "12201"))

    # database
    DB_NAME = os.getenv("DB_NAME", "postgres")
    DB_USER = os.getenv("DB_USER", "postgres")
    DB_PASS = os.getenv("DB_PASS", "postgres")
    DB_SERVICE = os.getenv("DB_SERVICE", "db")
    DB_PORT = os.getenv("DB_PORT", "5432")
    DATABASE_CONNECTION_URI = (
        f"postgresql+psycopg2://{DB_USER}:{DB_PASS}@{DB_SERVICE}:{DB_PORT}/{DB_NAME}"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # application
    API_BASE_URL = os.getenv("API_BASE_URL", "/flask-todo-api")


class DefaultConfig(BaseConfig):

    # Statement for enabling the development environment
    # DEBUG = True
    pass
