import datetime
import flask_sqlalchemy


db = flask_sqlalchemy.SQLAlchemy()


class Todos(db.Model):
    __tablename__ = "todos"
    id = db.Column(db.Integer, primary_key=True)
    todo = db.Column(db.String(500), nullable=False)
    created = db.Column(db.TIMESTAMP, default=datetime.datetime.now, nullable=False)
    owner = db.Column(db.String(33), nullable=False)
    priority = db.Column(db.SmallInteger, nullable=False, default=10)
    note = db.Column(db.Text, nullable=True)
    completed = db.Column(db.TIMESTAMP, nullable=True)

    def __str__(self):
        return f"{self.todo} ({self.priority}) - {self.created}"

    def to_json(self):
        return {
            "id": self.id,
            "todo": self.todo,
            "created": self.created.strftime("%Y-%m-%d %H:%M"),
            "owner": self.owner,
            "priority": self.priority,
            "note": self.note,
            "completed": self.completed.strftime("%Y-%m-%d %H:%M")
            if self.completed
            else None,
        }


class User(object):
    def __init__(self, username, password, avatar):
        # self.id = id
        self.username = username
        self.password = password
        self.avatar = avatar

    def __str__(self):
        return f"User(id='{self.username}')"


class LoginResponse(object):
    def __init__(self, status, message, user):
        self.status = status
        self.message = message
        self.user = user

    def __str_(self):
        return f"{self.status}, {self.message}, {self.user}"

    def to_json(self):
        return {"status": self.status, "message": self.message, "user": self.user}
