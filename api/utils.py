import logging
from flask import Flask

from models import db
import config as Config

logger = logging.getLogger("main_logger")


class PrefixMiddleware(object):
    def __init__(self, app, prefix=""):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):
        try:
            if environ["PATH_INFO"].startswith(self.prefix):
                environ["PATH_INFO"] = environ["PATH_INFO"][
                    len(self.prefix) :  # noqa: E203
                ]  # noqa: E203
                environ["SCRIPT_NAME"] = self.prefix
                return self.app(environ, start_response)
            else:
                start_response("404", [("Content-Type", "text/plain")])
                return ["This url does not belong to the app.".encode()]
        except Exception as e:
            logger.debug("An error occurred in adjusting the path:\n{0}".format(e))
            start_response("500", [("Content-Type", "text/plain")])
            return ["Path Error: {0}".format(e).encode()]


def create_app():
    flask_app = Flask(__name__)
    flask_app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = Config.DefaultConfig.DATABASE_CONNECTION_URI
    flask_app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    flask_app.config["DEBUG"] = True
    flask_app.app_context().push()
    db.init_app(flask_app)
    db.create_all()
    return flask_app
