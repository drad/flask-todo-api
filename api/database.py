from models import db


def get_all(model, amount=None):
    if amount:
        data = model.query.limit(amount)
    else:
        data = model.query.all()
    return data


def get_one(model, id):
    data = model.query.filter_by(id=id).first()
    return data.to_json() if data else None


def get_for_user(model, user, amount):
    if amount:
        data = model.query.filter_by(owner=user).limit(amount)
    else:
        data = model.query.filter_by(owner=user).all()
    return data


def search(model, todo):
    data = model.query.filter(model.todo.ilike(todo)).all()
    return data


def search_for_user(model, user, todo):
    data = model.query.filter_by(owner=user).filter(model.todo.ilike(todo)).all()
    return data


def add_instance(model, **kwargs):
    instance = model(**kwargs)
    db.session.add(instance)
    commit_changes()
    db.session.refresh(instance)
    return instance.id


def edit_instance(model, id, data):
    instance = model.query.filter_by(id=id).all()[0]
    for attr, new_value in data.items():
        if attr != "id":
            setattr(instance, attr, new_value)
    commit_changes()


def delete_instance(model, id):
    model.query.filter_by(id=id).delete()
    commit_changes()


def commit_changes():
    db.session.commit()
