#
# Authentication: ldap
#
import logging

from ldap3 import Server, Connection, ALL, SUBTREE
from models import User, LoginResponse
import config as Config

logger = logging.getLogger("main_logger")

server = Server(
    Config.DefaultConfig.LDAP_HOST,
    use_ssl=Config.DefaultConfig.LDAP_USE_SSL,
    get_info=ALL,
)


def get_roles(username):
    """Get roles for a user binding as the LDAP_BIND_USER.
    Return: list of roles that the user has (empty if no roles).
    """
    logger.debug(f"- check if user has needed role(s)...")
    ldap_groups = []
    conn = Connection(
        server,
        Config.DefaultConfig.LDAP_BIND_USER_DN,
        Config.DefaultConfig.LDAP_BIND_USER_PASSWORD,
    )

    try:
        conn.start_tls()
    except Exception as e:
        logger.error(f"ERROR: LDAP Server connection issue: {e}")
        return ldap_groups

    # perform the Bind operation
    if not conn.bind():
        logger.error(
            f"ERROR: in connecting as LDAP_BIND_USER_DN, please ensure LDAP is setup correctly: {conn.result}"
        )
        return ldap_groups

    # get all of the groups the user is a member of
    ldap_groups = []
    # check if user is in group
    groups_filter = f"(&(objectClass=*)(member=cn={username},{Config.DefaultConfig.LDAP_BASE_DN})(cn~={Config.DefaultConfig.LDAP_APP_GROUP_PREFIX}))"
    conn.search(
        f"ou=groups,{Config.DefaultConfig.LDAP_BASE_DN}",
        groups_filter,
        attributes=["cn"],
    )

    for entry in conn.entries:
        for role in entry:
            role_cleaned = str(role).replace(
                Config.DefaultConfig.LDAP_APP_GROUP_PREFIX, ""
            )
            ldap_groups.append(role_cleaned)

    return ldap_groups


def bind(username, password):
    """Bind (authenticate) as user.
    """

    logger.debug(f"- bind (authenticate) as user...")
    conn = Connection(
        server, f"cn={username},{Config.DefaultConfig.LDAP_BASE_DN}", password,
    )
    conn.start_tls()

    # perform the Bind operation
    if not conn.bind():
        logger.error(f"ERROR in authenticating: {conn.result}")
        return None

    # get user's details
    user_filter = f"(&(objectClass=*))"
    conn.search(
        f"cn={username},{Config.DefaultConfig.LDAP_BASE_DN}",
        user_filter,
        search_scope=SUBTREE,
        attributes=["cn", "sn", "displayName", "givenName", "mail", "labeledURI"],
    )
    response = conn.response[0]
    if not response:
        logger.error(f"ERROR: could not retrieve user details: {conn.response}")
        return None
    else:
        # build user object here...
        username = response["attributes"]["cn"][0]
        # first_name = response["attributes"]["givenName"][0]
        # last_name = response["attributes"]["sn"][0]
        # display_name = response["attributes"]["displayName"][0]
        # mail = response["attributes"]["mail"][0]
        avatar = None
        for item in response["attributes"]["labeledURI"]:
            k, v = item.split(":", maxsplit=1)
            avatar = v if k == "avatar" else None
        return User(username, "", avatar)


def login(username, password):
    """Authenticate against ldap
      Return: login_response - custom object with status, message and user object.
    """

    # check users roles.
    #  note: we check roles before authenticating user.
    roles = get_roles(username)
    if not roles:
        return LoginResponse(
            "error",
            "Access Denied: LDAP connectivity issue or User does not have permission",
            None,
        )

    # bind (authenticate) as user.
    user = bind(username, password)
    if not user:
        return LoginResponse(
            "error", "Access Denied: username / password did not match", None
        )

    return LoginResponse("success", "success", user)
