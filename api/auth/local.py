#
# Authentication: local
#
import logging

from werkzeug.security import safe_str_cmp

from models import User, LoginResponse

logger = logging.getLogger("main_logger")

users = [
    User(
        "user1",
        "abcde12345",
        "https://avataaars.io/?avatarStyle=Circle&topType=ShortHairDreads02&accessoriesType=Blank&hairColor=Black&facialHairType=BeardMagestic&facialHairColor=Black&clotheType=Overall&clotheColor=Red&eyeType=Side&eyebrowType=UpDownNatural&mouthType=Serious&skinColor=Light",
    ),
    User(
        "user2",
        "zxywv98765",
        "https://avataaars.io/?avatarStyle=Circle&topType=ShortHairDreads02&accessoriesType=Blank&hairColor=Black&facialHairType=Beard&facialHairColor=Black&clotheType=Overall&clotheColor=Red&eyeType=Side&eyebrowType=UpDownNatural&mouthType=Serious&skinColor=Dark",
    ),
]

username_table = {u.username: u for u in users}
# userid_table = {u.id: u for u in users}


def login(username, password):
    """Authenticate against local user object.
      Return: login_response - custom object with status, message and user object.
    """

    user = username_table.get(username, None)
    # check password
    if not user:
        return LoginResponse("error", "Access Denied: user does not exist", None)
    elif not safe_str_cmp(user.password.encode("utf-8"), password.encode("utf-8")):
        return LoginResponse(
            "error", "Access Denied: username / password did not match", None
        )

    return LoginResponse("success", "success", user)
